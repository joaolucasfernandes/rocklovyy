describe "Post/Signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "Valida status code" do
      expect(@result.code).to eql 200
    end

    it "Valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado que eu tenho um novo usuario
      payload = { name: "João da Silva", email: "joao@ig.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])

      # e o email ja foi cadastrado
      Signup.new.create(payload)
      # quando faço uma requisição para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  # nome é obrigatorio
  # email é obrigatorio
  # pass é obrigatorio

  examples = [
    {
      title: "Nome obrigatorio",
      payload: { name: "", email: "daniel@feliciano.com", password: "pwd123" },
      code: 412,
      error: "required name",
    },
    {
      title: "Duplicado",
      payload: { name: "Joao da Silva", email: "joao@ig.com.br", password: "pwd123" },
      code: 409,
      error: "Email already exists :(",
    },
    {
      title: "Email obrigatorio",
      payload: { name: "Joao da Silva", email: "", password: "pwd123" },
      code: 412,
      error: "required email",
    },
    {
      title: "Senha obrigatoria",
      payload: { name: "Joao da Silva", email: "joao@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },

  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "Valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "Valida id do usuário #{e[:code]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
