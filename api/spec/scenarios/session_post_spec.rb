describe "POST/sessions" do
  context "Login com sucesso" do
    before(:all) do
      payload = { email: "betao@hotmail.com", password: "pwd123" }
      @result = Sessions.new.login(payload)
    end

    it "Valida status code" do
      expect(@result.code).to eql 200
    end

    it "Valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  # examples = [
  #   {
  #     title: "Email incorreto",
  #     payload: { email: "daniel$feliciano.com", password: "pwd123" },
  #     code: 412,
  #     error: "wrong email",
  #   },
  #   {
  #     title: "Senha incorreta",
  #     payload: { email: "daniel@feliciano.com", password: "123456" },
  #     code: 401,
  #     error: "Unauthorized",
  #   },
  #   {
  #     title: "Usuario não existe",
  #     payload: { email: "daniel$feliciano.com", password: "12345" },
  #     code: 412,
  #     error: "wrong email",
  #   },
  #   {
  #     title: "Email em branco",
  #     payload: { email: "", password: "pwd123" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Senha em branco",
  #     payload: { email: "daniel$feliciano.com", password: "" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Sem nada",
  #     payload: { email: "", password: "" },
  #     code: 412,
  #     error: "required email",
  #   },
  #   {
  #     title: "Apenas o email",
  #     payload: { email: "daniel@feliciano.com" },
  #     code: 412,
  #     error: "required password",
  #   },
  #   {
  #     title: "Apenas a senha",
  #     payload: { password: "pwd123" },
  #     code: 412,
  #     error: "required email",
  #   },
  # ]

  examples = Helpers::get_fixture("login")

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "Valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "Valida id do usuário #{e[:code]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
