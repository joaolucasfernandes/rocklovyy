require_relative "base_api"

class Signup < BaseApi
  def create(payload)
    # payload = { email: email, password: pass } foi para o post_session
    return self.class.post(
             "/signup",
             body: payload.to_json,
             headers: { "Content-Type": "application/json" },
           )
  end
end
